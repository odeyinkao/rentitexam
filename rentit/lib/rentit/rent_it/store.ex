defmodule Rentit.RentIt.Store do
  use Ecto.Schema
  import Ecto.Changeset

  @derive {Poison.Encoder,
           only: [:id, :costperday, :description, :end_date, :name, :price, :start_date, :status]}
  schema "status" do
    field(:costperday, :float)
    field(:description, :string)
    field(:end_date, :date)
    field(:name, :string)
    field(:price, :float)
    field(:start_date, :date)
    field(:status, :string)

    timestamps()
  end

  @doc false
  def changeset(store, attrs) do
    store
    |> cast(attrs, [:start_date, :end_date, :name, :description, :costperday, :price, :status])
    |> validate_required([:start_date, :end_date, :name, :description, :costperday, :price, :status])
  end
end

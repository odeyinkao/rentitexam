defmodule RentitWeb.RentController do
  use RentitWeb, :controller
  import Ecto.Query, warn: false
  alias Rentit.Repo
  alias Rentit.RentIt.Store

  def index(conn, _params) do
    # IO.inspect({name})

    query =
      from(item in Store,
        where: item.name == ^_params["name"]
        select: item
      )

    store = Repo.all(query)

    IO.puts("==========")
    IO.inspect(_params["name"])
    IO.puts("==========")

    conn
    |> put_status(:ok)
    |> json(store)
  end

  def create(conn, %{"name" => name, "start_date" => start_date, "end_date" => end_date}) do
    # IO.puts("==========")
    # IO.inspect(name)
    # IO.puts("==========")
    %Ecto.Date{year: year, month: month, day: day}
    query =
      from(item in Store,
        where: item.name == ^name and not(star_date > ^star_date  and end_date < ^end_date),
        select: item
      )

    store = Repo.all(query)

    conn
    # |> resp
    |> put_status(200)
    |> json(store)
  end

  def update(conn, %{"id" => id}) do
    # Todo: getting a model as parameters
    IO.inspect("The query call")
    now = Date.utc_today()

    query =
      from(item in Loan,
        where:
          (item.code == "A" and item.id == ^id and item.time_extended < 4) or
          (item.code == "B" and item.id == ^id and item.time_extended < 7) or
          (item.code == "C" and item.id == ^id and item.time_extended < 14),
        # where: (item.enddate < ^now),
        # distinct: item.code,
        # order_by: item.startdate,
        # select: count(item)
        select: item
      )

    res = Repo.all(query)

    #  if length(res) > 0 do .... else do ....

    # res = Repo.one(query)
    case res do
      [item] ->
        endDate = Date.add(Date.utc_today(), 7)
        startDate = Date.utc_today()

        update = %{
          :time_extended => item.time_extended + 1,
          :startdate => startDate,
          :enddate => endDate
        }

        item |> Loan.changeset(update) |> Repo.insert_or_update()
        # IO.puts("==========")
        # IO.inspect(returned)
        # IO.puts("==========")

        conn
        |> put_status(:ok)
        |> json(update)

      [] ->
        conn
        |> put_status(404)
        |> json(%{"message" => "Item you are trying to work on break the stipulated rule"})

      _ ->
        conn
        # |> resp
        |> put_status(500)
        |> json(%{"message" => "Error at server inform the server Admin"})
    end

    # IO.inspect(query)
    # loan = Repo.get(Loan, id)

    # conn
    # |> put_status(404)
    # |> json(res)
  end
end

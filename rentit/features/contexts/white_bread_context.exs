defmodule WhiteBreadContext do
  use WhiteBread.Context
  use Hound.Helpers

  feature_starting_state(fn ->
    Application.ensure_all_started(:hound)
    %{}
  end)

  scenario_starting_state(fn _state ->
    Hound.start_session()
    %{}
  end)

  scenario_finalize(fn _status, _state ->
    nil
    # Hound.end_session
  end)

  given_ ~r/^that i alraedy borrowed the following books$/, fn state ->
    {:ok, state}
  end

  and_ ~r/^I want to extend "(?<argument_one>[^"]+)"$/,
fn state, %{argument_one: _argument_one} ->
  {:ok, state}
end

and_ ~r/^I open RentItWeb available web page$/, fn state ->
  {:ok, state}
end

when_ ~r/^I click on an Item with description "(?<argument_one>[^"]+)"$/,
fn state, %{argument_one: _argument_one} ->
  {:ok, state}
end

then_ ~r/^I see receive my Invoice with details ans cost$/, fn state ->
  {:ok, state}
end

end

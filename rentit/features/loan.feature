Feature: BookEquipment
    As a RentIt user
    Such that I want to help book an Item
    When i query i should see only available for period
  when i select the invoice should be created based on my period

  Scenario: Booking via RentItWeb web page
    Given that i alraedy borrowed the following books
      | name      | description              | costperday | start_date | end_date   |
      | Excavator | 15 Tonnes Mini excavator | 150.00     | 09-01-2019 | 16-01-2019 |
      | Excavator | 3 Tonnes Mini excavator  | 200.00     | 03-02-2019 | 06-02-2019 |

    And I want to extend "Excavator"
    And I open RentItWeb available web page
    When I click on an Item with description "15 Tonnes Mini excavator"
    Then I see receive my Invoice with details ans cost

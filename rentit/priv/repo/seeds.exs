# Script for populating the database. You can run it as:
#
#     mix run priv/repo/seeds.exs
#
# Inside the script, you can read and write to any of your
# repositories directly:
#
#     Rentit.Repo.insert!(%Rentit.SomeSchema{})
#
# We recommend using the bang functions (`insert!`, `update!`
# and so on) as they will fail if something goes wrong.

alias Rentit.RentIt.Store
alias Rentit.Repo



Repo.insert(%Store{
  status: "Free",
  start_date: Date.add(Date.utc_today(), 30),
  name: "Excavator",
  description: "15 Tonnes Mini excavator",
  end_date: Date.add(Date.utc_today(), 40),
  costperday: 150.00,
  price: 0.00
})


Repo.insert(%Store{
  status: "Free",
  start_date: Date.add(Date.utc_today(), 10),
  name: "Excavator",
  description: "3 Tonnes Mini excavator",
  end_date: Date.add(Date.utc_today(), 20),
  costperday: 200.00,
  price: 0.00
})

defmodule Rentit.Repo.Migrations.CreateStatus do
  use Ecto.Migration

  def change do
    create table(:status) do
      add(:start_date, :date)
      add(:end_date, :date)
      add(:name, :string)
      add(:description, :string)
      add(:costperday, :float)
      add(:price, :float)
      add(:status, :string)

      timestamps()
    end
  end
end

{application,rentit,
             [{applications,[kernel,stdlib,elixir,logger,runtime_tools,
                             gettext,poison,white_bread,hound,phoenix_pubsub,
                             postgrex,phoenix,phoenix_html,cors_plug,
                             plug_cowboy,phoenix_ecto]},
              {description,"rentit"},
              {modules,['Elixir.Rentit','Elixir.Rentit.Application',
                        'Elixir.Rentit.DataCase','Elixir.Rentit.RentIt.Store',
                        'Elixir.Rentit.Repo','Elixir.RentitWeb',
                        'Elixir.RentitWeb.ChannelCase',
                        'Elixir.RentitWeb.ConnCase',
                        'Elixir.RentitWeb.DataController',
                        'Elixir.RentitWeb.Endpoint',
                        'Elixir.RentitWeb.ErrorHelpers',
                        'Elixir.RentitWeb.ErrorView',
                        'Elixir.RentitWeb.Gettext',
                        'Elixir.RentitWeb.LayoutView',
                        'Elixir.RentitWeb.PageController',
                        'Elixir.RentitWeb.PageView','Elixir.RentitWeb.Router',
                        'Elixir.RentitWeb.Router.Helpers',
                        'Elixir.RentitWeb.UserSocket']},
              {registered,[]},
              {vsn,"0.0.1"},
              {mod,{'Elixir.Rentit.Application',[]}}]}.

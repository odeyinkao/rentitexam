import Vue from 'vue'
import './plugins/vuetify'
import App from './App.vue'
import router from './router'
import store from './store'
import 'roboto-fontface/css/roboto/roboto-fontface.css'
import 'material-design-icons-iconfont/dist/material-design-icons.css'
import VueResource from 'vue-resource'

Vue.config.productionTip = false
Vue.use(VueResource)
Vue.use(require('vue-moment'))
Vue.http.options.root = 'http://localhost:4000/api/'

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')

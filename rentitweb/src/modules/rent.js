import Vue from 'vue';
import router from '../router';

const state = {
  rents: [],
  invoice: {},
  error: {}
};

const mutations = {
  'SET_RENT'(state, data) {
    state.rents = data
  },
  'SET_INVOICE'(state, data) {
    state.invoice = data
  },
  'SET_ERROR'(state, data) {
    state.error = data
  }
};

const actions = {
  // loadAvailableRents: ({
  //   commit
  // }) => {
  //   Vue.http.get('loans')
  //     .then(data => {
  //       if (data) {
  //         commit('SET_RENT', data.body)
  //         commit('SET_ERROR', {})
  //         // console.log(data)
  //       }
  //     }, error => {
  //       commit('SET_RENT', {})
  //       commit('SET_ERROR', error.body)
  //     });
  // },

  requestForRent: ({
    commit,
    dispatch
  }, id) => {
    Vue.http.patch('rents/' + id)
      .then(data => {
        if (data) {
          commit('SET_INVOICE', data.body)
          commit('SET_ERROR', {})
          console.log(data.body)
        }
      }, error => {
        commit('SET_INVOICE', {})
        commit('SET_ERROR', error.body)
      })
  },

  queryAvailable: ({
    commit,
    dispatch
  }, data) => {
    Vue.http.post('loans/', data)
      .then(data => {
        if (data) {
          commit('SET_RENT', data.body)
          commit('SET_ERROR', {})
          console.log(data.body)
        }
      }, error => {
        commit('SET_RENT', [])
        commit('SET_ERROR', error.body)
      })
  }
}

const getters = {
  error: state => {
    return state.error
  },
  rents: state => {
    return state.rents
  },
  invoice: state => {
    return state.invoice
  }
}

export default {
  state,
  mutations,
  actions,
  getters
};
